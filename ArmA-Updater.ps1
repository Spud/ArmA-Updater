param (
    [Parameter(Mandatory = $false)]
    [switch]$Install,
    [Parameter(Mandatory = $false)]
    [switch]$HeadlessClient
)

$SteamModCollectionUrl = "http://steamcommunity.com/sharedfiles/filedetails/?id=1212198611"
$mod_root_folder = "C:\games\Arma3\A3Files\steamapps\workshop\content\107410"
$mod_folder_dest = "C:\games\Arma3\A3Master\"
$arma_key_dir = "C:\games\Arma3\A3Master\keys"
$arma_files = "C:\games\Arma3\A3Files"
    
Write-Host -foreground  magenta "####################################################################"
Write-Host -foreground  magenta "#######   Checking Powershell Execution Policy"
Write-Host -foreground  magenta "####################################################################`n"


if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit 
}
Write-Host -foreground  magenta "Opening a powershell window as Admin`n"

#here we will download the mods from steam and output it to a file that can be used for steamcmd
    
$_regExItems = "(?:SubscribeCollectionItem\( '(?'CollectionItem'\d+)')"
$_modlistHtml = Invoke-WebRequest $SteamModCollectionUrl | Select-Object -Expand Content
$_groups = ([regex]::Matches($_modlistHtml, $_regExItems)).Groups
$_items = $_groups  | Where-Object { $_.Name -like "CollectionItem" } | Select-Object -ExpandProperty Value
$_modlist = @()
for ($_i = 0; $_i -lt $_items.Count; $_i++) {
    
    $_modlist += "workshop_download_item 107410 $($_items[$_i]) validate$(if ($_i -eq $_items.Count -1) { " +quit" })"
}
$_modlist += "exit"
$_modlist | Set-Content "$arma_files\modlist.txt" -Force -Encoding ascii

    


#Check if folder is already there
if (!(Test-Path -Path "$mod_folder_dest")) {
    Write-Host -Foreground magenta "Installatie pad" $mod_folder_dest " bestaat niet, deze wordt nu aangemaakt"
    New-Item -Path "$mod_folder_dest" -ItemType Directory
}

#Check if folder is already there
if (!(Test-Path -Path "$arma_files")) {
    Write-Host -Foreground magenta "Installatie pad" $arma_files "bestaat niet, deze wordt nu aangemaakt"
    New-Item -Path "$arma_files" -ItemType Directory
}


Write-Host -foreground  magenta "####################################################################"
Write-Host -foreground  magenta "#######   Arma 3 update script for JAGC by Spud"
Write-Host -foreground  magenta "####################################################################`n"

$SteamUserName = Read-Host -Prompt 'Enter Steam Username'


if ($Install -eq $true) {
    #Check if steamCMD is already present
    if (!(Test-Path -Path "$arma_files\steamcmd.exe") ) {   
        #download steamcmd latest version
        Invoke-WebRequest https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip -Outfile $arma_files\steamcmd.zip
        Expand-Archive -Force "$arma_files\steamcmd.zip" "$arma_files"
        #install steamcmd
        & $arma_files\steamcmd.exe +quit
    }
}

#Update Arma 3 Server
Write-Host -foreground  magenta "####################################################################"
Write-Host -foreground  magenta "#######   Updating/Installing Arma 3 Server"
Write-Host -foreground  magenta "####################################################################`n"


& $arma_files\steamcmd.exe +login $SteamUserName +force_install_dir $mod_folder_dest +"app_update 233780" validate +quit


#update mods
Write-Host -foreground  magenta "####################################################################"
Write-Host -foreground  magenta "#######   Updating/Installing Arma 3 Mods"
Write-Host -foreground  magenta "####################################################################`n"


& $arma_files\steamcmd.exe +login $SteamUserName +runscript .\modlist.txt +quit

#update mods
Write-Host -foreground  magenta "####################################################################"
Write-Host -foreground  magenta "#######   Creating links from Workshop folder to Arma directory"
Write-Host -foreground  magenta "####################################################################`n"


#here we will copy the keys and create the sylinks
foreach ($mod_folder IN (Get-ChildItem $mod_root_folder -Directory | select-object -expandproperty FullName)) {
    if (Test-Path "$mod_folder\meta.cpp") {
        $content = Get-Content "$mod_folder\meta.cpp" # parse the meta.cpp and find the publishedID  
        $modname = $content[2].Split("=")[1].trim().trimend(";").trim('"')
        $mod_key_dir = "$mod_folder_dest\@$modname\keys"		
        $mod_key_dir2 = "$mod_folder_dest\@$modname\key"	
        cmd /c mklink /D "$mod_folder_dest\@$modname" "$mod_folder" #Create symlink
            
        Write-Host -foreground  magenta "####################################################################"
        Write-Host -foreground  magenta "#######   Creating link....."
        Write-Host -foreground  magenta "####################################################################`n"
        if ($HeadlessClient -eq $false) {
            Write-Host -foreground  magenta "####################################################################"
            Write-Host -foreground  magenta "#######   Copying keys....."
            Write-Host -foreground  magenta "####################################################################`n"
            cmd /c copy /Y "$mod_key_dir\*" "$arma_key_dir" #copy keys to key folder      
            cmd /c copy /Y "$mod_key_dir2\*" "$arma_key_dir" #copy keys to keyS folder            
        }
    }
}
$x = 1

if ($Install -eq $true) {
    Write-Host -foreground  magenta "####################################################################"
    Write-Host -foreground  magenta "#######   Opening Firewall Ports 2302-2305 for Arma TCP/UDP"
    Write-Host -foreground  magenta "####################################################################`n"
    
    New-NetFirewallRule -DisplayName "ARMA JAGC/UDP X64" -Direction Inbound -Protocol UDP -LocalPort 2302-2305 -Action allow -Program "$mod_folder_dest\arma3server_x64.exe"
    New-NetFirewallRule -DisplayName "ARMA JAGC/UDP" -Direction Inbound -Protocol UDP -LocalPort 2302-2305 -Action allow -Program "$mod_folder_dest\arma3server.exe"
    New-NetFirewallRule -DisplayName "ARMA JAGC/TCP X64" -Direction Inbound -Protocol TCP -LocalPort 2302-2305 -Action allow -Program "$mod_folder_dest\arma3server_x64.exe"
    New-NetFirewallRule -DisplayName "ARMA JAGC/TCP" -Direction Inbound -Protocol TCP -LocalPort 2302-2305 -Action allow -Program "$mod_folder_dest\arma3server.exe"
}

#cleanup
#Remove-Item $arma_files\modlist.txt
#Remove-Item $arma_files\mods.txt

Write-Host -foreground  magenta  "Check of alles goed gegaan lijkt te zijn, en druk op een toets om door te gaan"
$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")

#Get-NetFirewallRule | where DisplayName -like "ARMA JAGC*" | ft
