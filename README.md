# Synopsis

Arma Install/Update Script

This Script is designed to make the Arma server installation more easy.
And to give an easy way to update the server and all mods.

It wil also configure firewall rules 2302-2305 TCP/UDP

This can also be used to for headless client installations/updates.

## Prerequisite

Microsoft .NET Framework 4.5.2
http://www.microsoft.com/en-us/download/details.aspx?id=42642

Visual C++ Redistributable Packages for Visual Studio 2013
https://www.microsoft.com/en-us/download/details.aspx?id=40784

DirectX
https://www.microsoft.com/nl-nl/download/details.aspx?id=35

## Parameters

### `Install`

Use this parameter to Install and Download Arma 3 server, SteamCMD, modlist and mods.
Without this parameter the system will only update the current Arma 3 server, modlist and mods.

### `HeadlessClient`

This prevents the system from copying the mod keys to Arma directory

## Code Examples

### Example
Launch a PowerShell Console as Administrator:

Arma-Updater.ps1 -Install -HeadlessClient

## Motivation

Because Bohemia Interactive doesnt provide simple tools for this

## Installation

Copy the script somewhere to your server and run it.

By default, Arma will be installed in the following folders

```powershell
#This is the workshop mod folder location.
$mod_root_folder="C:\games\Arma3\A3Files\steamapps\workshop\content\107410"
#This is the Arma installation directory.
$mod_folder_dest="C:\games\Arma3\A3Master\"
#This is the Arma keys directory.
$arma_key_dir="C:\games\Arma3\A3Master\keys"
#this is the location where steamcmd resides.
$arma_files="C:\games\Arma3\A3Files"
```

## HeadlessClient.bat
Feel free to use this default configuration file to create a headlessclient shortcut.


## Contributors

None yet, but feel free to log any issues and/or fork the repository, make the change in your fork and make a merge request.

## TODO

- Check if Firewall rules are already present/dont add them if true.
- Custom installation paths.

## License

`Made by Spud for JAGC.NL`
